package examples;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;

public class ButtonAndEventHandling extends Application{
	public static void main(String[] args){
	Application.launch(args);
	}
	public void start(Stage primaryStage){
		primaryStage.setTitle("Buttons and Event Handling");
		FlowPane root = new FlowPane(10, 10);

		Scene scene = new Scene(root, 300, 100);
		Label response = new Label("Push a button.");

		Button btnFirst = new Button("First");
		Button btnSecond = new Button("Second");

		btnFirst.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent e){
				response.setText("The first button was pressed.");
			}
		});

		btnSecond.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent e){
				response.setText("The second button was pressed.");
			}
		});

		root.getChildren().addAll(btnFirst, btnSecond, response);

		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
