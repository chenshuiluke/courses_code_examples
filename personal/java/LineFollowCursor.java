package linefollowcursor;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.scene.input.MouseEvent;

public class LineFollowCursor extends Application{
		public static void main(String[] args){
			Application.launch(args);
		}
		public void start(Stage primaryStage){
			primaryStage.setTitle("LineFollowCursor");

			Group root = new Group();
			Scene scene = new Scene(root, 300, 150, Color.WHITE);

			Line blackLine = new Line(10, 10, 200, 10);
			blackLine.setStroke(Color.BLACK);
			blackLine.setStrokeWidth(5);

			Line redLine = new Line(20, 20, 200, 10);
			redLine.setStroke(Color.RED);
			redLine.setStrokeWidth(5);


			scene.setOnMouseMoved(new EventHandler<MouseEvent>(){
				public void handle(MouseEvent event){
					blackLine.setEndX(event.getX());
					blackLine.setEndY(event.getY());
					redLine.setEndX(event.getX());
					redLine.setEndY(event.getY());
				}
			});
			root.getChildren().add(blackLine);
			root.getChildren().add(redLine);
			primaryStage.setScene(scene);
			primaryStage.show();
		}
}
