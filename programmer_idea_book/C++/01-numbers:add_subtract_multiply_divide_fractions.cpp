//Requires C++ 11
/*
Title: Add/Subtract/Multiply/Divide Fractions
Difficulty: 5
Description: This project involves working with fractions. How do you add 1⁄3 + 1⁄5? Create a
program that first asks the user which operation they want to do: add, subtract, multiply or divide
and then asks for 1, 2 or more fractions to work with. The program prints out the result.
Tips: Think about how you might divide up the fraction itself into its numerator and denominator.
Many of the operations involve finding a common denominator, getting its reciprocal and
possibly reducing a fraction to lowest terms. This would be much easier if you can isolate the
numerator from its denominator. You could also do this using a custom fraction class.
Added Difficulty: Try working with mixed, improper, or complex fractions.
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<int> findPosOfAllSlashesAndSpaces(string fraction){
	vector<int> pos;

	pos.push_back(0);
	pos.push_back(0);

	//The first two elements represent the # of / and the number of spaces respectively

	for(int counter = 0; counter < fraction.length(); counter++){
		if(fraction[counter] == '/'){
			pos[0] = counter;
		}
		else if(fraction[counter] == ' '){
			pos[1] = counter;
		}
	}
	return pos;
}
vector<float> getFractions(){
	vector <float> fractions;
	string fraction;

	cout << "Please enter the fractions you want to operate on. Enter a blank line to continue." << endl;
	do{
		getline(cin, fraction);
		if(fraction != ""){
			vector<int> slashPositions = findPosOfAllSlashesAndSpaces(fraction);
			/*
				Everything returned from findPosOfAllSlashesAndSpaces has a size of at least 2
				here, we check to see if there are > 0 and <= 2 slaashes
			*/
			if(slashPositions[0] > 0){

				float proper = 0;
				if(slashPositions[1] > 0){
					proper = stof(fraction.substr(0, slashPositions[1]));
				}
				cout << "slash pos" << slashPositions[0] << "\t space pos" << slashPositions[1] << "\tlength " << fraction.length() << endl;
				float numerator = stof(fraction.substr(slashPositions[1], slashPositions[0]));
				float denominator = stof(fraction.substr(slashPositions[0]+1, fraction.length()));
				cout << numerator << "/" << denominator << endl;
				fractions.push_back(proper + (numerator/denominator));

			}
		}
	}while(fraction != "");
	return fractions;
}
void operate(string operation){
	vector<float> fractions = getFractions();
	float result = fractions[0];
	for(int counter = 1; counter < fractions.size(); counter++){
		if(operation == "add")
			result+=fractions[counter];
		else if(operation == "subtract")
			result-=fractions[counter];
		else if(operation == "multiply")
			result*=fractions[counter];
		else if(operation == "divide")
			result/=fractions[counter];
	}
	cout << "Result :\t" << result << endl;
}
int main(){
	string choice;
	do{
		cout << "Please enter the number associated with the operation you want to perform." << endl;
		cout << "1. Add" << endl << "2. Subtract" << endl << "3. Multiply"<< endl << "4. Divide" << endl << "5. Exit" << endl;
		getline(cin, choice);
		if(choice == "1"){
			operate("add");
		}
		if(choice == "2"){
			operate("subtract");
		}
		if(choice == "3"){
			operate("multiply");
		}
		if(choice == "4"){
			operate("divide");
		}
	}while(choice!="5");
}
