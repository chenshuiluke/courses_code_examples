/*
Title: Binary to Decimal and Back Converter
Difficulty: 4
Description: Make a program which has the user enter in either a decimal value and prints out
its binary equivalent or enter a binary value and it prints out the decimal equivalent value.
Tips: Start testing with some low well known decimal and binary values to make sure that it is
first converting correctly. Use values you can easily calculate like 3, 4 and 5. Try taking a
number, dividing it by 2 (using integer division) and recording its remainder. Do remember that
N % 2 is always going to come out as either 1 or 0. Take the result of the divide and divide that
by 2 again. Keep doing this until you can no longer divide it by 2 (again using integer division).
*/

import java.util.Scanner;

public class BinaryToDecimalAndBackConverter{
	static String convertDecimalToBinary(String decimal){
			int integer = Integer.valueOf(decimal);
			String result = new String();
			while(integer > 0){
				result+=String.valueOf(integer%2);
				integer/=2;
			}
			result = new StringBuffer(result).reverse().toString();
			return result;
	}
	static int convertBinaryToDecimal(String binary){
			int result = 0;
			for(int reverseCounter = 0; reverseCounter < binary.length(); reverseCounter++){
				char currentChar = binary.charAt(binary.length() - reverseCounter - 1);
				int numericValue = Character.getNumericValue(currentChar);
				result+=Math.pow(2, reverseCounter) * numericValue;
//				System.out.println(Math.pow(2, reverseCounter) * Character.getNumericValue(binary.charAt(binary.length() - reverseCounter - 1)) + " reverseCounter " + reverseCounter);
			}
			return result;

	}
	static String isDecimalOrBinary(String input){
		for(int counter = 0; counter < input.length(); counter++){
			if(input.charAt(counter) != '0' && input.charAt(counter) != '1'){
				return "decimal";
			}
		}
		return "binary";
	}
	public static void main(String[] args){
		Scanner inputScanner = new Scanner(System.in);
		System.out.print("Please enter the binary/decimal number to convert:");
		String input = inputScanner.nextLine();
		switch(isDecimalOrBinary(input)){
			case "decimal":
				System.out.println(convertDecimalToBinary(input));
			break;
			case "binary":
				System.out.println(convertBinaryToDecimal(input));
			break;
		}
	}
}
