/*
Title: Guestbook / Journal
Difficulty: 6
Description: Develop an application where the user can keep track of journal entries or create a
public guestbook where multiple people can write in it to say “there were here”. This application
lends itself perfectly for online sites but could certainly be done as a stand-alone application. It
is also possible that this guestbook / journal could reside on a network and people can access it
from multiple computers. The program should keep track of the date and time of a post, allow
the user to browse through various days, edit/delete offensive posts or queue posts before
showing publicly.
Tips: This is a classic application in information management. How you store this information
will be dependent on the technologies you use or the scalability you desire. It could be a simple
text file, it could be a huge database or perhaps somewhere in between. Have the interface take
in the post information (perhaps an author’s name) and their message. Make sure you validate
the post and render its information harmless. One way to do this is by substituting dangerous
characters (look up the concept of “code injection” if you are unfamiliar with the idea) or
escaping it.
*/

public class Journal{
	public static void main(String[] args){
		System.out.println("Please choose an option: ");
		System.out.println("1. Add a journal entry.");
		System.out.println("2. View journal entries.");
	}
}
