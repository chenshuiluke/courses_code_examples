/*
Title: Next Prime Number
Difficulty: 2
Description: Ask the user to enter a number and have the program find the next prime number
after that value. If you don’t remember, a prime number is a number which can only be divided
by 1 and itself. Thus if the user enters 3, have the program find the next prime number (5) and
print it out.
Tips: There are many ways to find primes. One of the simplest ways to find a prime is to simply
start from the number the user enters and loop through the numbers one by one testing if it is
prime. You can test if it is prime by trying to divide that number by all numbers starting from 2
until the square root of that number. If none divide evenly, it is prime. For example, to test if 11
is prime, we take the square root of it (which is a little over 3) and we test from 2 to 3. If none of
them divide evenly (test the remainder of the division) then it is prime. Since 2 nor 3 divide
evenly into 11, it is prime. Test 12... square root is again a little over 3 so we move from 2 to 3
and see if any divide evenly. Since
*/
import java.util.Scanner;

public class NextPrimeNumber{
	static long getNextPrimeNumber(long start){
		boolean isPrime = false;
		long current = start+1;
		for(;!isPrime; current++){
			boolean hasDivisorOtherThanOneAndItself = false;
			long root = Double.valueOf(Math.sqrt(current)).longValue();
			for(long counter = 2; counter <= root; counter++){
				if(current % counter == 0){
					hasDivisorOtherThanOneAndItself = true;
//					System.out.println(counter);
					break;
				}
			}
			isPrime = !hasDivisorOtherThanOneAndItself;
		}
		return current-1;
	}
	public static void main(String[] args){
		long number = 0L;
		String input;

		Scanner inputScanner = new Scanner(System.in);

		do{
			System.out.print("Please enter the number:\t");
			input = inputScanner.nextLine();
			if(input.equals("")){
				break;
			}
			number = Math.abs(Long.valueOf(input));
			System.out.println("Next prime number:\t" + getNextPrimeNumber(number));
		}while(true);
		inputScanner.close();

	}
}
