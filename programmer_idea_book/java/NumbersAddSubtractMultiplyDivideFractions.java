//Requires C++ 11
/*
Title: Add/Subtract/Multiply/Divide Fractions
Difficulty: 5
Description: This project involves working with fractions. How do you add 1⁄3 + 1⁄5? Create a
program that first asks the user which operation they want to do: add, subtract, multiply or divide
and then asks for 1, 2 or more fractions to work with. The program prints out the result.
Tips: Think about how you might divide up the fraction itself into its numerator and denominator.
Many of the operations involve finding a common denominator, getting its reciprocal and
possibly reducing a fraction to lowest terms. This would be much easier if you can isolate the
numerator from its denominator. You could also do this using a custom fraction class.
Added Difficulty: Try working with mixed, improper, or complex fractions.
*/
import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;

public class NumbersAddSubtractMultiplyDivideFractions{
	public static double parseFraction(String input){
		String[] numeratorAndDenom = input.split("[/]+");
		String[] wholeNumAndNumer = input.split("[ ]+");

		if(wholeNumAndNumer.length == 1){
			wholeNumAndNumer[0] = "0";
		}
		if(numeratorAndDenom.length > 2){
			System.out.println("Too many /'s.");
		}
		else if(numeratorAndDenom.length < 2){
			System.out.println("Too few /'s.");
		}
		else if(wholeNumAndNumer.length > 2){
			System.out.println("Too many spaces.");
		}
		else{
			String[] numeratorSplit = numeratorAndDenom[0].split("[ ]+");
			numeratorAndDenom[0] = numeratorSplit[numeratorSplit.length-1];
			return Double.parseDouble(wholeNumAndNumer[0]) + (Double.parseDouble(numeratorAndDenom[0]) / Double.parseDouble(numeratorAndDenom[1]));
		}
		return -9783.9783;
	}
	public static double operate(String mode){
		ArrayList<Double> fractionsList =  new ArrayList<Double>();
		String fraction = new String("i");
		Scanner inputScanner = new Scanner(System.in);
		while(!fraction.equals("")){
			System.out.println("Please enter the fraction or simply press the enter key to finish entering fractions.");
			if(inputScanner.hasNextLine()){
				fraction = inputScanner.nextLine();
				if(fraction.equals("")){
					break;
				}
				fractionsList.add(parseFraction(fraction));
			}
			//System.out.println(fractionsList.toString());
		}
		double result = 0;
		int counter = 0;
		if(mode.equals("subtract") || mode.equals("multiply") || mode.equals("divide")){
			result = fractionsList.get(0);
			counter = 1;
		}
		for(; counter < fractionsList.size(); counter++){
			if(mode.equals("add")){
				result+=fractionsList.get(counter);
			}
			else if(mode.equals("subtract")){
				result-=fractionsList.get(counter);
			}
			else if(mode.equals("multiply")){
				result*=fractionsList.get(counter);
			}
			else if(mode.equals("divide")){
				result/=fractionsList.get(counter);
			}
		}
		return result;
	}
	public static void main(String[] args){
		String input = new String();
		while(!input.equals("5")){
			//System.out.print(input);
			Scanner inputScanner = new Scanner(System.in);
			System.out.println("Please enter the number of your selected option:");
			System.out.println("1. Add");
			System.out.println("2. Subtract");
			System.out.println("3. Multiply");
			System.out.println("4. Divide");
			System.out.println("5. Exit");

			if(inputScanner.hasNextLine()){
				input = inputScanner.nextLine();
			//	System.out.println(input);
				switch(input){
					case "1":
							System.out.println(operate("add"));
					break;
					case "2":
							System.out.println(operate("subtract"));
					break;
					case "3":
							System.out.println(operate("multiply"));
					break;
					case "4":
							System.out.println(operate("divide"));
					break;
				}
			}
		}
	}
}
