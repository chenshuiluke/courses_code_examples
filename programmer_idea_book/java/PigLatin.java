/*
Title: Pig Latin - Simple
Difficulty: 4
Description: Ask the user to enter in a string which will then be printed back to screen in Pig
Latin. If the user enters “hello world” it will be printed back to them as “ello-hay orld-way”.
*/
import java.util.Scanner;
public class PigLatin{
	static String pigLatin(String input){
		StringBuilder pigLatin = new StringBuilder(input);

		if(input.charAt(0) == 'a' || input.charAt(0) == 'e' || input.charAt(0) == 'i' ||
		 input.charAt(0) == 'o' || input.charAt(0) == 'u'){
			pigLatin.append("-ay");
		 }
		 else{
			pigLatin.deleteCharAt(0);
			pigLatin.append("-" + input.charAt(0) + "ay");
		 }
		return pigLatin.toString();
	}
	public static void main(String[] args){
		Scanner inputScanner = new Scanner(System.in);
		String[] input = inputScanner.nextLine().split(" ");
		for(int counter = 0; counter < input.length; counter++){
			System.out.print(pigLatin(input[counter]) + " ");
		}
		System.out.println("");
	}
}
